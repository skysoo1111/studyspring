
import org.junit.Test;
import spring.dao.UserDao;
import spring.domain.User;

import java.sql.SQLException;

public class DBActionTest {
    UserDao userDao = new UserDao();
    User user = new User();

    @Test
    public void dbAddTest() throws SQLException, ClassNotFoundException {
        user.setId("whiteship");
        user.setName("박성수");
        user.setPassword("married");

        userDao.add(user);

        System.out.println(user.getId() + " 등록 성공");

    }

    @Test
    public void dbGetTest() throws SQLException, ClassNotFoundException {
        user.setId("whiteship");
        User user2 = userDao.get(user.getId());
        System.out.println(user2.toString() + " 조회 성공");
    }

    @Test
    public void dbDeleteTest() throws SQLException, ClassNotFoundException {
        user.setId("whiteship");
        userDao.delete(user.getId());
        System.out.println("삭제 성공");

    }

}