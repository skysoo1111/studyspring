package spring.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//public class PostgreDao extends UserDao{
public class PostgreDao implements ConnectionMaker {
    String dburl = "jdbc:postgresql://192.168.10.7:5432/dwarf";
    String dbuser = "postgres";
    String dbpassword = "postgres";

    @Override
    public Connection getConnection() throws SQLException, ClassNotFoundException {
//            Class.forName("org.postgresql.Driver");
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection(dburl,dbuser,dbpassword);
            return c;
    }
}
