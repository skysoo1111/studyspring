package spring.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//public class MySqlDao extends UserDao {
public class MySqlDao implements ConnectionMaker {
    String dburl = "jdbc:mysql://localhost:3306/dwarf?characterEncoding=UTF-8&serverTimezone=UTC";
    String dbuser = "root";
    String dbpassword = "root";

    @Override
    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection c = DriverManager.getConnection(dburl,dbuser,dbpassword);
        return c;
    }
}
