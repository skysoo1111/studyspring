package spring;

import java.sql.SQLException;

public class SpringTobyApplication {

    private static final SpringTobyApplication O = new SpringTobyApplication();
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        SpringTobyApplication.getInstance()._main(args);
    }


    SpringTobyApplication _main(final String[] args) throws SQLException, ClassNotFoundException {

        return this;
    }

    public static SpringTobyApplication getInstance(){
        return O;
    }


}
