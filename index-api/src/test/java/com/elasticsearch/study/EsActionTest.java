package com.elasticsearch.study;

import com.elasticsearch.study.dto.BookModel;
import com.elasticsearch.study.dto.Category;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import com.elasticsearch.study.repository.BookRepository;
import com.elasticsearch.study.service.BookSearchService;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
* ES 데이터 적재 및 조 테스트
* @author skysoo
* @version 1.0.0
* @since 2019-10-08 22:11회
**/

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class EsActionTest {

  BookSearchService bookSearchService;

  @Autowired
  @Qualifier("bookRepository")
  BookRepository bookRepository;

  @Before
  public void setup() {
    bookSearchService = new BookSearchService(bookRepository);
  }

  @Test
  public void esConnectionTest() throws UnknownHostException {
    String host= "192.168.1.4";
    int port =9300;
    String clusterName = "docker-cluster";

      Settings settings = Settings.builder().put("cluster.name", clusterName).build();

      TransportClient client = new PreBuiltTransportClient(settings);
      client.addTransportAddress(new TransportAddress(InetAddress.getByName(host), port));

      assertTrue(client != null);
    }

  @Test
  public void validFind() {
    List<BookModel> list = bookSearchService.findAll();

    assertNotNull(list);
  }

  @Test
  public void validSave() {
    Exception ex = null;

    try {
      bookSearchService.save(BookModel.builder().id("2").title("Java Spring MVC").author("이순신").build());
    } catch (Exception exception) {
      ex = exception;
    }

    assertTrue(null == ex);
  }

  @Test
  public void validFindByTitle() {
    Exception ex = null;

    try {
      BookModel bookModel = bookSearchService.findByTitle("Java Spring MVC");

      assertThat(bookModel, is(IsNull.notNullValue()));
      System.out.println(bookModel.toString());
    } catch (Exception exception) {
      ex = exception;
    }

    assertTrue(null == ex);
  }
}
