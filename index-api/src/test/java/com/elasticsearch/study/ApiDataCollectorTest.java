package com.elasticsearch.study;

import com.elasticsearch.study.configuration.BatchCollector;
import com.elasticsearch.study.dto.Category;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
* API 데이터 수집 테스트 소스
* @author skysoo
* @version 1.0.0
* @since 2019-10-09 19:55
**/

@Slf4j
@RunWith(SpringRunner.class)
@SpringBatchTest
public class ApiDataCollectorTest {

    RestTemplate restTemplate = new RestTemplate();

    @Test
    public void fetchTest(){
        String serviceKey = "22c14dfb3ce67e11fefc964d411a05c9";
        String dataUrl = "http://www.nl.go.kr/app/nl/search/openApi/search.jsp";

//        batchCollector.fetchBookDataFromAPI(Category.dan,"Java");
        try {
            URI uri = new URI(dataUrl + "?key=" + serviceKey + "&category=" +Category.dan+"&kwd="+"Java");

            ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri,String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode root = objectMapper.readTree(responseEntity.getBody());

            log.info("Data API # {}",root);
        } catch (URISyntaxException e) {
            log.error("{} URL error",e);
        } catch (IOException e) {
            log.error("{} Response error",e);
        }
    }

}
