package com.elasticsearch.study.dto;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * 공공데이터 category 목록
 * dan : 단행자료
 * yon : 연속자료
 * media : 멀티미디어자료
 * disabled : 장애인자료
 * web : 웹정보자료
 * map : 지도자료
 * music : 악보자료
 * etc : 기타자료
 * archive : 해외수집기록물
 * cip : 출판예정도서
 * korcis : 한국고전적종합물
 *
 * @author skysoo
 * @version 1.0.0
 * @since 2019-10-09 20:26
 **/

public enum Category {
    dan, yon, media, disabled, web, map, music, etc, archive, cip, korcis

}
