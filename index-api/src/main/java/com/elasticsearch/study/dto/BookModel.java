package com.elasticsearch.study.dto;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
*
* @author skysoo
* @version 1.0.0
* @since 2019-10-08 22:10
**/

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(indexName = "book", type = "book")
@Builder
public class BookModel {

    @Id
    private String id;
    private String title;
    private String author;
    private long startAt;
    private long endAt;

}
