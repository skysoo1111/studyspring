package com.elasticsearch.study;

import afu.org.checkerframework.checker.oigj.qual.O;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
*
* @author skysoo
* @version 1.0.0
* @since 2019-10-08 22:10
**/

@SpringBootApplication
public class StudyApplication {

  private static StudyApplication O = new StudyApplication();

  public static void main(String[] args) {

    StudyApplication.getInstance()._main(args);

    System.out.println("이건 뭐");
  }

  public static StudyApplication getInstance(){
    return O;
  }

  StudyApplication _main(String[] args){
    SpringApplication.run(StudyApplication.class, args);

    return this;
  }

}

