package com.elasticsearch.study.service;

import com.elasticsearch.study.repository.BookRepository;
import com.google.common.collect.Lists;
import com.elasticsearch.study.dto.BookModel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
*
* @author skysoo
* @version 1.0.0
* @since 2019-10-08 22:11
**/

@AllArgsConstructor
@NoArgsConstructor
@Service
public class BookSearchService {

  private BookRepository bookRepository;

  public void save(BookModel book) {
    bookRepository.save(book);
  }

  public List<BookModel> findAll() {
    return Lists.newArrayList(bookRepository.findAll());
  }

  public BookModel findByTitle(String title) {
    return bookRepository.findByTitle(title);
  }

}
