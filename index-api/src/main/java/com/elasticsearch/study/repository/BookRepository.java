package com.elasticsearch.study.repository;

import com.elasticsearch.study.dto.BookModel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
*
* @author skysoo
* @version 1.0.0
* @since 2019-10-08 22:10
**/

@Repository("bookRepository")
public interface BookRepository extends ElasticsearchRepository<BookModel, String> {

  BookModel findByTitle(String title);

}
