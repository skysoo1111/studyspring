package com.elasticsearch.study.configuration;

import com.elasticsearch.study.dto.BookModel;
import com.elasticsearch.study.dto.Category;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.awt.print.Book;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
*
* @author skysoo
* @version 1.0.0
* @since 2019-10-08 23:40
**/

@Slf4j // log 사용을 위한 lombok annotaion
@RequiredArgsConstructor // 생성자 DI를 위한 lombok annotation
@Configuration
public class BatchCollector {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private RestTemplate restTemplate;

    @Value("${open.api.public.data.servicekey}")
    private String serviceKey;
    @Value("${open.api.public.data.bookurl}")
    private String dataUrl;
    private int chunkSize = 1;




    @Bean
    public Job esBatchJob() {
        return jobBuilderFactory.get("esBatchJob")
                .start(esBatchStep())
                .build();
    }

    @Bean
    public Step esBatchStep(){
        return stepBuilderFactory.get("esBatchStep")
                .<BookModel,BookModel>chunk(chunkSize)
                .reader(exItemReader())
                .writer(exItemWrter())
                .build();
    }

    @Bean
    public ItemReader<BookModel> exItemReader() {
        return null;
    }

    private ItemWriter<BookModel> exItemWrter() {
        return null;

    }

    public void fetchBookDataFromAPI(Category category, String kwd){
        try {
            URI uri = new URI(dataUrl + "?key=" + serviceKey + "&category=" +category+"&kwd="+kwd);

            ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri,String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode root = objectMapper.readTree(responseEntity.getBody());

            log.info("Data API # {}",root);
        } catch (URISyntaxException e) {
            log.error("{} URL error",e);
        } catch (IOException e) {
            log.error("{} Response error",e);
        }
    }


}
