# 조회시
POST http://localhost:8080/book/search
{
    "title": "AI",
    "querytype": "match"
}

- title : 검색하고 싶은 키워드를 제목 대상으로 검색
- querytype : match / term 


# 적재시
POST http://localhost:8080/book/index
{
    "certKey": "certKey",
    "pageNo": "1",
    "pageSize": "100",
    "resultStyle": "json",
    "startPublishDate": "20190708",
    "endPublishDate": "20190808"
}

certKey는 **http://www.nl.go.kr/nl/service/open/api_info.jsp** 이 사이트에서 개인적으로 발급 받아야 한다.